package main

import (
	"fmt"
	"math"
)

// Block describes any facilities on the apartment's building
// these are the example with 3 facilities, but it could be a lot of facilities
// we need to find the best location of the apartmen's location based on those facilities defined
type Block struct {
	Gym    bool
	School bool
	Store  bool
}

func findMinimumDistance(blocks []Block, reqs []string) int {
	findMinimum := func(arr []int) int {
		minVal := math.MaxInt64
		index := -1

		for i, val := range arr {
			if val < minVal {
				minVal = val
				index = i
			}
		}

		return index
	}
	result := make([]int, len(blocks))
	for i := range result {
		result[i] = math.MinInt64
	}

	for currentIdx := range blocks {
		steps := make([]int, 0)
		for _, req := range reqs {
			step := math.MaxInt64

			// Compare from end to start
			for compareIdx := currentIdx - 1; compareIdx >= 0; compareIdx-- {
				if getFacilityValue(blocks[compareIdx], req) {
					step = int(math.Min(float64(step), float64(currentIdx-compareIdx)))
				}
			}

			// Compare from start to end
			for compareIdx := currentIdx; compareIdx < len(blocks); compareIdx++ {
				if getFacilityValue(blocks[compareIdx], req) {
					step = int(math.Min(float64(step), float64(compareIdx-currentIdx)))
				}
			}

			steps = append(steps, step)

			result[currentIdx] = int(math.Max(float64(result[currentIdx]), float64(step)))
		}
		fmt.Printf("Block %d: %v = %d\n", currentIdx, steps, result[currentIdx])
	}

	return findMinimum(result)
}

func getFacilityValue(block Block, facility string) bool {
	switch facility {
	case "gym":
		return block.Gym
	case "school":
		return block.School
	case "store":
		return block.Store
	default:
		return false
	}
}

func main() {
	blocks := []Block{
		{Gym: false, School: true, Store: false},
		{Gym: true, School: false, Store: false},
		{Gym: true, School: true, Store: false},
		{Gym: false, School: true, Store: false},
		{Gym: false, School: true, Store: true},
	}

	requirements := []string{"gym", "school", "store"}

	result := findMinimumDistance(blocks, requirements)
	fmt.Println("Block with the most minimum distance to all facilities: Block", result)
}
